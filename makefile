.PHONY: build-image
.PHONY: run

run: build-image local-docker

build-image:
	docker build -t lysol-game . 

local-docker:
	docker run -p 8080:80 -e DB_HOST="soy_un_host" -e DB_USER="soy_un_user" -e DB_PASS="soy_una_pass" -e DB_NAME="soy_un_nombre" lysol-game

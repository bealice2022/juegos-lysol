## Server Requirements

- BD MySQL
- Php > 7.0
- Free space > 40 mb

## Instalation and Execution


**To generate database**

Run "create.sql" file which is a script that generates the required database and tables.

**To build the image**

``
docker build -t imageName
``

*You should pass the following environment variables in order to setup your mysql server: DB_HOST, DB_USER, DB_PASS, DB_NAME (default "lysoljuegos"). You can change the default value of variables in "api/config.php"*


**To run it**

``
docker run -p 8080:80 -e DB_HOST="host" -e DB_USER="user" -e DB_PASS="pass" -e DB_NAME="name" imageName
``

**To run it locally** (the env values can be changed in the makefile)


``
make run
``

## Endpoints

**Game**

In order to relate the player to "jugadas" table in the db you have to pass the the player user id (uid) as a parameter of the request:

For instance, if the user id is 10, the request will be: 
.../juegos/index.html?uid=10

**Health**

You can monitor the status of the game in /health.php (if it reads "OK" it works)





------------------------------------------------------------------

## Requerimientos del Servidor

- BD MySQL
- Php > 7.0
- Espacio disponible > 40 mb

## Instalación y Ejecución


**Para generar la base de datos**

Correr el archivo "create.sql" el cual es un script que genera la base de datos y tablas generadas.

**Para buildear la imagen**

``
docker build -t imageName
``

*Deben pasar las siguientes variables de entorno para configurar su mysql server: DB_HOST, DB_USER, DB_PASS, DB_NAME (default "lysoljuegos"). Pueden cambiar el valor default de estas variables en "api/config.php"*


**Para correrlo**

``
docker run -p 8080:80 -e DB_HOST="host" -e DB_USER="user" -e DB_PASS="pass" -e DB_NAME="name" imageName
``

**Para correrlo localmente** (los valores de entorno pueden cambiarse dentro del makefile)


``
make run
``

## Endpoints

**Juego**

Para relacionar los jugadores a la tabla "jugadas" en la base de datos se debe pasar el user id del jugador (uid) como un parametro de la request:

Por ejemplo, si el user id es 10, la request sería: 
.../juegos/index.html?uid=10

**Health**

Pueden monitorear el estado del juego en /health.php (si se lee "OK" funciona)


<?php

	header('Access-Control-Allow-Origin: *');
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');

	//-------------------------------------------------------------------------------------------------
	// Db
	
	$db_host = getenv("DB_HOST") ?: "";
	$db_user = getenv("DB_USER") ?: "";
	$db_pass = getenv("DB_PASS") ?:"";
	$db_dbname = getenv("DB_NAME") ?: "lysoljuegos";

	//-------------------------------------------------------------------------------------------------
?>

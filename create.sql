CREATE DATABASE `lysoljuegos` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

CREATE TABLE `jugadas` (
  `idjugadas` int(11) NOT NULL AUTO_INCREMENT,
  `juego` varchar(45) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `puntos` int(11) DEFAULT NULL,
  `tiempo` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `mensaje` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idjugadas`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
